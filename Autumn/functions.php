<?php
session_start();

if(PHP_VERSION < 7.2){
	if(!is_admin()){
		wp_die('Autumn 主题需要PHP 7.2，你的服务器 PHP 版本为：'.PHP_VERSION.'，请升级到 PHP 7.2。');
		exit;
	}
}elseif(!defined('WPJAM_BASIC_PLUGIN_FILE')){
	if(!is_admin()){
		wp_die('Autumn 主题基于 WPJAM Basic 插件开发，请先<a href="https://wordpress.org/plugins/wpjam-basic/">下载</a>并<a href="'.admin_url('plugins.php').'">激活</a> WPJAM Basic 插件。');
		exit;
	}
}else{

	function wpjam_theme_get_setting($setting_name){
		return wpjam_get_setting('wpjam_theme', $setting_name);
	}
	
	include_once TEMPLATEPATH.'/public/hooks.php';
	include_once TEMPLATEPATH.'/public/comment.php';
	include_once TEMPLATEPATH.'/public/ajax.php';
	include_once TEMPLATEPATH.'/public/api.php';
	include_once TEMPLATEPATH.'/public/widgets.php';

	if(is_admin()){
		include(TEMPLATEPATH.'/admin/admin.php');
	}
}

add_theme_support('title-tag');
add_theme_support('post-thumbnails');
add_theme_support('post-formats', ['gallery','video','audio']);

register_nav_menus(['main'	=> '主菜单']);

register_sidebar([
	'name'			=> '全站侧栏',
	'id'			=> 'widget_right',
	'before_widget'	=> '<section class="widget %2$s">', 
	'after_widget'	=> '</section>', 
	'before_title'	=> '<h5 class="widget-title">', 
	'after_title'	=> '</h5>' 
]);

