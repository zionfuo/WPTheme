<?php $paging = wpjam_theme_get_setting('paging_xintheme');

$current_page	= get_query_var('paged') ?: 1;

if($paging == '1'){
	wpjam_pagenavi();
}elseif($paging == '3'){
	global $wp_query;
	// 如果没有更多文章 不显示按钮
	if ($wp_query->max_num_pages > $current_page)
		echo '<div class="infinite-scroll-action"><div class="xintheme-loadmore infinite-scroll-button button">加载更多</div></div>';
 }elseif( $paging == '4' ){
	global $wp_query;
	// 如果没有更多文章 不显示按钮
	if ( $wp_query->max_num_pages > $current_page)
		echo '<div class="infinite-scroll-action"><div class="xintheme-loadmore infinite-scroll-button button">加载更多</div></div>';
}elseif( $paging == '2' ){ ?>
<nav class="navigation posts-navigation">
	<div class="nav-links">
		<div class="nav-previous">
			<?php next_posts_link('下一页 »') ?>
		</div>
		<div class="nav-next">
			<?php previous_posts_link('« 上一页') ?>
			<?php if ( is_paged() ) { ?>
			<script type="text/javascript">
			document.getElementById("body").className="paged-previous paged-next"; 
			</script>
			<?php } ?>
		</div>
	</div>
</nav>
<?php }