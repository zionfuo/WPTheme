<?php 
if(is_module('user')){
	$list_region	= 'col_3';
}else{
	$list_region	= wpjam_theme_get_setting('list_region');
}

?>
<?php if($list_region == 'list' ) { ?>
<article class="excerpt">
	<a class="focus" href="<?php the_permalink(); ?>">
		<img class="lazyloaded" data-srcset="<?php echo wpjam_get_post_thumbnail_url($post,array(520,350), $crop=1);?>" srcset="<?php echo wpjam_get_post_thumbnail_url($post,array(520,350), $crop=1);?>">
	</a>
	<header>
		<?php the_category(' ');?>
		<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	</header>
	<p class="meta">
		<span class="author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')) ?>"><i class="iconfont icon-yonghu"></i><?php echo get_the_author(); ?></a></span>

		<time><i class="iconfont icon-shijian"></i> <?php the_time('Y-m-d') ?></time>

		<?php if(wpjam_theme_get_setting('list_read')){ ?><span class="pv"><i class="iconfont icon-liulan"></i> <?php echo wpjam_get_post_views(get_the_ID()); ?></span><?php } ?>

		<?php if(wpjam_theme_get_setting('list_comment')){?><a class="comment" href="<?php the_permalink(); ?>#comments"><i class="iconfont icon-pinglun"></i><span class="count"> <?php echo $post->comment_count; ?></span></a><?php } ?>

		<?php if(wpjam_theme_get_setting('list_like')){ ?><?php wpjam_post_like_button(get_the_ID());?><?php } ?>
			
		<a class="share" href="<?php the_permalink(); ?>" data-url="<?php the_permalink(); ?>" data-title="<?php the_title(); ?>" data-thumbnail="<?php echo wpjam_get_post_thumbnail_url($post,array(150,150), $crop=1);?>" data-image="<?php echo wpjam_get_post_thumbnail_url($post,array(1130,848), $crop=1);?>"><i class="iconfont icon-icon_share_normal"></i> <span>分享</span></a>
	</p>
	<p class="note"><?php the_excerpt(); ?></p>
</article>

<?php }else{ ?>

<article class="col-md-6 col-lg-4 <?php if($list_region == 'col_3' ) { echo 'col-xl-4'; }elseif($list_region == 'col_4' ){ echo 'col-xl-3'; }else{ echo 'col-xl-3'; } ?> grid-item">
	
	<?php $feature_list	= get_post_meta(get_the_ID(), 'feature_list', true); ?>

	<div class="post <?php if($feature_list){ echo 'cover lazyloaded'; } ?>" <?php if($feature_list){ echo 'style="background-image: url(\''. wpjam_get_post_thumbnail_url($post,array(840,1120), $crop=1).'\');"'; } ?>>

		<div class="entry-media with-placeholder" style="padding-bottom: 61.904761904762%;">
			<?php if(!$feature_list) {?>
			<a href="<?php the_permalink(); ?>">
				<img class="lazyloaded" data-srcset="<?php echo wpjam_get_post_thumbnail_url($post,array(420,260), $crop=1);?>" srcset="<?php echo wpjam_get_post_thumbnail_url($post,array(420,260), $crop=1);?>">
			</a>
			<?php } ?>
			<?php if(has_post_format( 'gallery' )) { //相册 ?>
			<div class="entry-format">
				<i class="iconfont icon-xiangce"></i>
			</div>
			<?php } else if ( has_post_format( 'video' )) { //视频 ?>
			<div class="entry-format">
				<i class="iconfont icon-shipin"></i>
			</div>
			<?php } else if ( has_post_format( 'audio' )) { //音频 ?>
			<div class="entry-format">
				<i class="iconfont icon-yinpin"></i>
			</div>
			<?php } ?>
		</div>
		<div class="entry-wrapper">
			<header class="entry-header">
			<div class="entry-category"><?php the_category(' ');?></div>
			<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
			</header>
			<div class="entry-excerpt">
				<p><?php the_excerpt();?></p>
			</div>
			<?php if(wpjam_theme_get_setting('list_author')){ ?>
			<div class="entry-author">
				<?php echo get_avatar( get_the_author_meta('ID'), '200' );?>
				<div class="author-info">
					<a class="author-name" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ) ?>"><?php echo get_the_author() ?></a>
					<span class="entry-date"><time datetime="<?php the_time('Y-m-d h:m:s') ?>"><?php the_time('Y-m-d') ?></time></span>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="entry-action">
			<div>
				<?php if(is_module('user')){?>
				<?php if($post->post_status == 'publish' ){ ?>
				<span class="status published">已发布</span>
				<?php }else if($post->post_status == 'pending' ){ ?>
				<span class="status">等待审核</span>
				<?php }else if($post->post_status == 'draft' ){ ?>
				<span class="status">草稿</span>
				<?php } ?>
				<?php if($post->post_status == 'draft' ){  ?>
				<span class="status"><a style="color:var(--accent-color)" href="<?php echo home_url(user_trailingslashit('/user/contribute'));?>?post_id=<?php echo $post->ID; ?>"><i class="iconfont icon-zuozhe" style="font-size: 14px;"></i> 编辑文章</a></span>
				<?php } ?>
				<?php } ?>

				<?php if(wpjam_theme_get_setting('list_like')){ ?><?php wpjam_post_like_button(get_the_ID());?><?php } ?>

				<?php if(wpjam_theme_get_setting('list_read')){ ?><a class="view" href="<?php the_permalink(); ?>"><i class="iconfont icon-liulan"></i><span class="count"><?php echo wpjam_get_post_views(get_the_ID()); ?></span></a><?php } ?>

				<?php if(wpjam_theme_get_setting('list_comment')){ ?><a class="comment" href="<?php the_permalink(); ?>#comments"><i class="iconfont icon-pinglun"></i><span class="count"><?php echo $post->comment_count; ?></span></a><?php } ?>
			</div>
			<div>
				<a class="share" href="<?php the_permalink(); ?>" data-url="<?php the_permalink(); ?>" data-title="<?php the_title(); ?>" data-thumbnail="<?php echo wpjam_get_post_thumbnail_url($post,array(150,150), $crop=1);?>" data-image="<?php echo wpjam_get_post_thumbnail_url($post,array(1130,848), $crop=1);?>"><i class="iconfont icon-icon_share_normal"></i><span>Share</span></a>
			</div>
		</div>
	</div>
	<?php if($feature_list) {?><a class="u-permalink" href="<?php the_permalink(); ?>"></a><?php } ?>
</article>
<?php }
