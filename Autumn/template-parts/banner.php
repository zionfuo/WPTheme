<?php if(!get_query_var('paged')){ ?>

<?php 
$sticky_posts	= get_option('sticky_posts');

if ($sticky_posts) {
	$slide_region	= wpjam_theme_get_setting('slide_region'); 
	$slide_query	= wpjam_query([
		'posts_per_page'		=> wpjam_theme_get_setting('slide_number') ?: 10,
		'post__in'				=> $sticky_posts,
		'ignore_sticky_posts'	=> 1
	]);
	
	if($slide_query->have_posts()){
?>

<?php if ( $slide_region == '4' ) { ?>
<div class="featured-wrapper lazyload" data-bg="<?php echo wpjam_theme_get_setting('slide_bg_img');?>">
<?php } ?>

<div class="container mobile">
<?php if( $slide_region == '1' ){?>
	<div class="featured-posts v1 owl-carousel with-padding">
		<?php while($slide_query->have_posts()){ $slide_query->the_post();?>
		<article class="featured-post lazyload visible" data-bg="<?php echo wpjam_get_post_thumbnail_url($post,array(1130,400), $crop=1);?>">
		<div class="entry-wrapper">
			<header class="entry-header">
				<div class="entry-category"><?php  the_category(' ');?></div>
				<h2 class="entry-title"><?php the_title(); ?></h2>
			</header>
			<div class="entry-excerpt"><p><?php the_excerpt();?></p></div>
			<div class="entry-author">
				<?php echo get_avatar( get_the_author_meta('ID'), '200' );?>
				<div class="author-info">
					<a class="author-name" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author() ?></a>
					<span class="entry-date"><time datetime="<?php the_time('Y-m-d h:m:s') ?>"><?php the_time('Y-m-d') ?></time></span>
				</div>
			</div>
		</div>
		<a class="u-permalink" href="<?php the_permalink(); ?>"></a>
		</article>
		<?php } ?>
	</div>
<?php }elseif( $slide_region == '2' ){?>
	<div class="featured-posts v2 owl-carousel with-padding">
		<?php while($slide_query->have_posts()){ $slide_query->the_post();?>
		<article class="featured-post lazyload visible">
		<div class="entry-wrapper">
			<div class="entry-thumbnail">
				<img class="lazyload" data-src="<?php echo wpjam_get_post_thumbnail_url($post,array(150,150), $crop=1);?>">
			</div>
			<header class="entry-header">
			<div class="entry-category">
				<div class="entry-category"><?php  the_category(' ');?></div>
				<h2 class="entry-title"><?php the_title(); ?></h2>
			</header>
			<div class="entry-excerpt">
				<p><?php the_excerpt();?></p>
			</div>
		</div>
		<a class="u-permalink" href="<?php the_permalink(); ?>"></a>
		</article>
		<?php }?>
	</div>
<?php }elseif( $slide_region == '3' ){?>
	<div class="featured-posts v3 owl-carousel with-padding">
		<?php while($slide_query->have_posts()){ $slide_query->the_post();?>
		<article class="featured-post lazyload visible" data-bg="<?php echo wpjam_get_post_thumbnail_url($post,array(840,560), $crop=1);?>">
		<div class="entry-wrapper">
			<header class="entry-header">
				<div class="entry-category"><?php  the_category(' ');?></div>
				<h2 class="entry-title"><?php the_title(); ?></h2>
			</header>
		</div>
		<a class="u-permalink" href="<?php the_permalink(); ?>"></a>
		</article>
		<?php } ?>
	</div>
<?php }elseif( $slide_region == '4' ){?>
	<div class="featured-posts v2 owl-carousel with-padding">
		<?php while($slide_query->have_posts()){ $slide_query->the_post();?>
		<article class="featured-post lazyload visible">
		<div class="entry-wrapper">
			<div class="entry-thumbnail">
				<img class="lazyload" data-src="<?php echo wpjam_get_post_thumbnail_url($post,array(150,150), $crop=1);?>">
			</div>
			<header class="entry-header">
				<div class="entry-category"><?php  the_category(' ');?></div>
				<h2 class="entry-title"><?php the_title(); ?></h2>
			</header>
			<div class="entry-excerpt">
				<p><?php the_excerpt();?></p>
			</div>
		</div>
		<a class="u-permalink" href="<?php the_permalink(); ?>"></a>
		</article>
		<?php }?>
	</div>
<?php }else{?>
	<div class="featured-posts v1 owl-carousel with-padding">
		<?php while($slide_query->have_posts()){ $slide_query->the_post();?>
		<article class="featured-post lazyload visible" data-bg="<?php echo wpjam_get_post_thumbnail_url($post, $size, $crop);?>">
		<div class="entry-wrapper">
			<header class="entry-header">
				<div class="entry-category"><?php  the_category(' ');?></div>
				<h2 class="entry-title"><?php the_title(); ?></h2>
			<div class="entry-meta">
			</div>
			</header>
			<div class="entry-excerpt">
				<p><?php the_excerpt(); ?></p>
			</div>
			<div class="entry-author">
				<?php echo get_avatar( get_the_author_meta('email'), '200' );?>
				<div class="author-info">
					<a class="author-name" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ) ?>"><?php echo get_the_author() ?></a>
					<span class="entry-date"><time datetime="<?php the_time('Y-m-d h:m:s') ?>"><?php the_time('Y-m-d') ?></time></span>
				</div>
			</div>
		</div>
		<a class="u-permalink" href="<?php the_permalink(); ?>"></a>
		</article>
		<?php } ?>
	</div>
<?php } ?>

<?php wp_reset_query(); ?>

</div>

<?php if ( $slide_region == '4' ) { ?>
</div>
<?php } ?>

<?php } ?>

<?php if( ( wpjam_theme_get_setting('index_cat')== '2' ) ) { ?>
<div class="container mobile">
	<style>.category-boxes {margin-top: 0}</style>
	<div class="category-boxes owl-carousel with-padding">
	<?php 
	if($cat_ids= wpjam_theme_get_setting('index_cat_id')){

	foreach ($cat_ids as $cat_id ){
	
	$cat = get_category($cat_id);

	if($cat){ ?>

	<div class="category-box">
		<div class="entry-thumbnails">
		<?php 
		$cat_posts_query = wpjam_query([
			'posts_per_page'		=> 3,
			'ignore_sticky_posts'	=> true,
			'cat'					=> $cat_id
		]);
		if($cat_posts_query->have_posts()){ $i = 0;
			?>
			<?php while($cat_posts_query->have_posts()){ $cat_posts_query->the_post(); $i++; global $post;?>
			<?php if($i == 1){ ?>
				<div class="big thumbnail">
					<img class="lazyload" data-src="<?php echo wpjam_get_post_thumbnail_url($post,array(420,280), $crop=1);?>">
				</div>
			<?php }elseif($i == 2){ ?>
				<div class="small">
				
				<div class="thumbnail">
					<img class="lazyload" data-src="<?php echo wpjam_get_post_thumbnail_url($post,array(150,150), $crop=1);?>">
				</div>
			<?php }elseif($i == 3){ ?>
				<div class="thumbnail">
					<img class="lazyload" data-src="<?php echo wpjam_get_post_thumbnail_url($post,array(150,150), $crop=1);?>">
					<span><?php echo $cat->count; ?> 篇文章</span>
				</div>
			<?php } ?>
			<?php } wp_reset_query(); ?>
			</div>
		<?php } ?>
		</div>
		<div class="entry-content">
			<div class="left">
				<h3 class="entry-title"><?php echo $cat->name; ?></h3>
			</div>
			<div class="right">
				<a class="arrow" href="<?php echo get_category_link($cat_id);?>"><i class="iconfont icon-zuo"></i></a>
			</div>
		</div>
		<a class="u-permalink" href="<?php echo get_category_link($cat_id);?>"></a>
	</div>

	<?php } } } } ?>
	</div>
</div>
<?php }

}