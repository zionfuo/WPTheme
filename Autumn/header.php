<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<?php 
wp_head();

$favicon		= wpjam_theme_get_setting('favicon') ?: get_template_directory_uri().'/static/images/favicon.ico';
$theme_color	= wpjam_theme_get_setting('theme_color') ?: '#f16b6f';
?>
<link rel="shortcut icon" href="<?php echo $favicon;?>"/>
<style type='text/css'>
html{--accent-color:<?php echo $theme_color;?>}
<?php if( wpjam_theme_get_setting('title_hidden') && !is_single() && !is_page()){ ?>
.entry-header .entry-title {text-overflow: ellipsis;white-space: nowrap;overflow: hidden;}
<?php } ?>
</style>
</head>
<body id="body" <?php body_class(); ?>>
<div class="site">
	<header class="site-header">
	<div class="container">
		<div class="navbar">
			<div class="branding-within">
				<?php if( $logo = wpjam_theme_get_setting('logo') ) { ?>
				<a class="logo" href="<?php echo home_url(); ?>" rel="home"><img src="<?php echo $logo;?>" alt="<?php echo get_bloginfo('name'); ?>"></a>
				<?php }else{ ?>
				<a class="logo text" href="<?php echo home_url(); ?>" rel="home"><?php echo get_bloginfo('name'); ?></a>
				<?php }?>
			</div>
			<nav class="main-menu hidden-xs hidden-sm hidden-md">
			<ul id="menu-primary" class="nav-list u-plain-list">
				<?php wp_nav_menu(['container'=>false, 'items_wrap'=>'%3$s', 'theme_location'=>'main']); ?>
			</ul>
			</nav>
			<?php if( wpjam_theme_get_setting('social') ) { ?>
			<?php }else{ ?>
			<?php if ( ! wp_is_mobile() ) {?>
			<div class="search-open navbar-button">
				<i class="iconfont icon-sousuo"></i>
			</div>
			<?php if( wpjam_theme_get_setting('navbar_user') ) { ?>
			<div class="main-nav">
			<?php if ( is_user_logged_in() ) { ?>
				<a class="dahuzi-land cd-signin" href="<?php echo home_url(user_trailingslashit('/user'));?>">用户中心</a>
			<?php }else{?>
				<a class="dahuzi-land cd-signin" href="javascript:void(0);">登陆</a>
			<?php }?>
			</div>
			<?php } ?>
			<style>.iconfont.icon-sousuo{font-size: 20px;font-weight: 700}.search-open.navbar-button{background-color: #fff;border: none;}</style>
			<?php } ?>
			<?php } ?>
			<div class="main-search">
				<form method="get" class="search-form inline" action="<?php bloginfo('url'); ?>">
					<input type="search" class="search-field inline-field" placeholder="输入关键词进行搜索…" autocomplete="off" value="" name="s" required="true">
					<button type="submit" class="search-submit"><i class="iconfont icon-sousuo"></i></button>
				</form>
				<div class="search-close navbar-button">
					<i class="iconfont icon-guanbi1"></i>
				</div>
			</div>
			<div class="col-hamburger hidden-lg hidden-xl">
				<div class="hamburger">
				</div>
				<div class="search-open navbar-button">
					<i class="iconfont icon-sousuo"></i>
				</div>
				<?php if( wpjam_theme_get_setting('navbar_user') ) { ?>
				<div class="main-nav">
				<?php if ( is_user_logged_in() ) { ?>
					<a class="dahuzi-land cd-signin" href="<?php echo home_url(user_trailingslashit('/user'));?>">用户中心</a>
				<?php }else{?>
					<a class="dahuzi-land cd-signin" href="javascript:void(0);">登陆</a>
				<?php }?>
				</div>
				<?php } ?>
			</div>
			<?php if( wpjam_theme_get_setting('social') ) { ?>
			<div class="col-social hidden-xs hidden-sm hidden-md">
				<div>
					<div class="social-links">
						<?php if( $autumn_weibo = wpjam_theme_get_setting('autumn_weibo') ) : ?>
						<a href="<?php echo $autumn_weibo; ?>" title="微博" target="_blank" rel="nofollow">
							<i class="iconfont icon-weibo"></i>
						</a>
						<?php endif; ?>
						<?php if( $autumn_qq = wpjam_theme_get_setting('autumn_qq') ) : ?>
						<a href="http://wpa.qq.com/msgrd?v=3&uin=<?php echo $autumn_qq; ?>&site=qq&menu=yes" title="QQ" target="_blank" rel="nofollow">
							<i class="iconfont icon-QQ"></i>
						</a>
						<?php endif; ?>
						<?php if( wpjam_theme_get_setting('autumn_weixin') ) : ?>
						<a id="tooltip-f-weixin" href="javascript:void(0);" title="微信">
							<i class="iconfont icon-weixin"></i>
						</a>
						<?php endif; ?>
						<?php if( $autumn_mail = wpjam_theme_get_setting('autumn_mail') ) : ?>
						<a href="http://mail.qq.com/cgi-bin/qm_share?t=qm_mailme&email=<?php echo $autumn_mail; ?>" title="QQ邮箱" target="_blank" rel="nofollow">
							<i class="iconfont icon-youxiang"></i>
						</a>
						<?php endif; ?>
					</div>
					<div class="search-open navbar-button">
						<i class="iconfont icon-sousuo"></i>
					</div>
					<?php if( wpjam_theme_get_setting('navbar_user') ) { ?>
					<div class="main-nav">
					<?php if ( is_user_logged_in() ) { ?>
						<a class="dahuzi-land cd-signin" href="<?php echo home_url(user_trailingslashit('/user'));?>">用户中心</a>
					<?php }else{?>
						<a class="dahuzi-land cd-signin" href="javascript:void(0);">登陆</a>
					<?php }?>
					</div>
					<?php } ?>
				</div>
			</div>
			<?php }else{ ?>
			<?php if ( wp_is_mobile() ) {?>
			<div class="col-social hidden-xs hidden-sm hidden-md">
				<div>
					<div class="search-open navbar-button">
						<i class="iconfont icon-sousuo"></i>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php } ?>
		</div>
	</div>
	</header>
<div class="off-canvas">
	<div class="mobile-menu">
	</div>
	<div class="close">
		<i class="iconfont icon-guanbi1"></i>
	</div>
</div>