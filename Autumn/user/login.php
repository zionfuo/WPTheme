<script>window._WPJAM_XinTheme = {uri: '<?php echo get_bloginfo("template_url") ?>'}</script>
<div class="cd-user-modal sign" id="sign">
	<div class="cd-user-modal-container">
		<ul class="cd-switcher">
			<li><a href="javascript:void(0);">用户登陆</a></li>
			<li><a href="javascript:void(0);">新用户注册</a></li>
		</ul>
		<div id="cd-login">
			<form id="login" class="cd-form" action="<?php echo get_option('home'); ?>/wp-login.php" method="post" novalidate="novalidate">
				<div class="sign-tips"></div>
				<p class="fieldset">
					<label class="image-replace cd-username" for="username">用户名</label>
					<input class="full-width has-padding has-border" id="username" type="text" placeholder="请输入用户名" name="username" required="" aria-required="true">
				</p>
				<p class="fieldset">
					<label class="image-replace cd-password" for="password">密码</label>
					<input class="full-width has-padding has-border" id="password" type="password" placeholder="输入密码..." name="password" required="" aria-required="true">
					<a href="javascript:void(0);" class="hide-password">显示</a>
				</p>
				<p class="fieldset">
					<input type="checkbox" id="remember-me" checked="checked" id="rememberme" value="forever">
					<label for="remember-me">记住密码</label>
					<!--a class="cd-form-bottom-message" href="javascript:void(0);">忘记密码？</a-->
					<a href="<?php echo get_option('home'); ?>/wp-login.php?action=lostpassword">忘记密码？</a>
				</p>
				<p class="fieldset">
					<input class="submit login-loader full-width" type="submit" value="登录" name="submit">
					<input type="hidden" name="action" value="WPJAM_XinTheme_login">
				</p>
				<input type="hidden" id="security" name="security" value="<?php echo  wp_create_nonce( 'security_nonce' );?>">
				<input type="hidden" name="_wp_http_referer" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
			</form>
			<!-- <a href="#0" class="cd-close-form">Close</a> -->
		</div>
		<div id="cd-signup">
			<form class="cd-form" id="register" action="<?php bloginfo('url'); ?>/wp-login.php?action=register" method="post" novalidate="novalidate">
				<div class="sign-tips"></div>
				<p class="fieldset">
					<label class="image-replace cd-username" for="user_name">用户名</label>
					<input class="input-control full-width has-padding has-border" id="user_name" type="text" name="user_name" placeholder="输入英文用户名" required="" aria-required="true">
				</p>
				<p class="fieldset">
					<label class="image-replace cd-email" for="user_email">E-mail</label>
					<input class="input-control full-width has-padding has-border" id="user_email" type="email" name="user_email" placeholder="输入常用邮箱" required="" aria-required="true">
				</p>
				<p class="fieldset">
					<label class="image-replace cd-password" for="user_pass">密码</label>
					<input class="input-control full-width has-padding has-border" id="user_pass" type="password" name="user_pass" placeholder="密码最小长度为6" required="" aria-required="true">
					<a href="javascript:void(0);" class="hide-password">显示</a>
				</p>
				<p class="fieldset">
					<label class="image-replace cd-password" for="user_pass2">再次输入密码</label>
					<input class="input-control full-width has-padding has-border" id="user_pass2" type="password" name="user_pass2" placeholder="密码最小长度为6" required="" aria-required="true">
					<a href="javascript:void(0);" class="hide-password">显示</a>
				</p>
				<p class="fieldset" id="captcha_inline">
					<input class="input-control inline full-width has-border" id="captcha" type="text" name="captcha" placeholder="输入验证码" required="">
					<span class="captcha-clk inline">获取验证码</span>
				</p>
				<!--p class="fieldset">
					<input type="checkbox" id="accept-terms">
					<label for="accept-terms">我同意这些 <a href="#0">条款</a></label>
				</p-->
				<p class="fieldset">
					<input type="hidden" name="action" value="WPJAM_XinTheme_register">
					<input class="full-width has-padding submit inline register-loader" type="submit" value="立即注册" name="submit">
				</p>
				<input type="hidden" id="user_security" name="user_security" value="<?php echo  wp_create_nonce( 'user_security_nonce' );?>"><input type="hidden" name="_wp_http_referer" value="<?php echo $_SERVER['REQUEST_URI']; ?>"> 
			</form>
			<!-- <a href="#0" class="cd-close-form">Close</a> -->
		</div>
		<!--div id="cd-reset-password">
			<p class="cd-form-message">
				输入您的电子邮件地址，您将收到一个创建新密码的链接。
			</p>
			<form class="cd-form">
				<p class="fieldset">
					<label class="image-replace cd-email" for="reset-email">E-mail</label>
					<input class="full-width has-padding has-border" id="reset-email" type="email" placeholder="输入您的E-mail账号...">
					<span class="cd-error-message">这里是错误信息!</span>
				</p>
				<p class="cd-form-bottom-message">
					<a href="javascript:void(0);">返回登陆</a>
				</p>
				<p class="fieldset">
					<input class="full-width has-padding" type="submit" value="重置密码">
				</p>
			</form>
		</div-->
		<a href="#0" class="cd-close-form">Close</a>
	</div>
</div>