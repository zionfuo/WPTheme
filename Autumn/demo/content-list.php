<article class="excerpt">
	<a class="focus" href="<?php the_permalink(); ?>">
		<img class="lazyloaded" data-srcset="<?php echo wpjam_get_post_thumbnail_url($post,array(520,350), $crop=1);?>" srcset="<?php echo wpjam_get_post_thumbnail_url($post,array(520,350), $crop=1);?>">
	</a>
	<header>
		<?php  
			$category = get_the_category();
			if($category[0]){
			echo '<a class="cat" href="'.get_category_link($category[0]->term_id ).'" rel="category">'.$category[0]->cat_name.'<i></i></a>';
			};
		?>
		<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	</header>
	<p class="meta">
		<span class="author"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ) ?>"><i class="iconfont icon-yonghu"></i> <?php echo get_the_author() ?></a></span>
		<time><i class="iconfont icon-shijian"></i> <?php the_time('Y-m-d') ?></time>
		<?php if( wpjam_get_setting('wpjam_theme', 'list_read') ) : ?><span class="pv"><i class="iconfont icon-liulan"></i> <?php echo wpjam_get_post_views(get_the_ID()); ?></span><?php endif; ?>
		<?php if( wpjam_get_setting('wpjam_theme', 'list_comment') ) : ?><a class="comment" href="<?php the_permalink(); ?>#comments"><i class="iconfont icon-pinglun"></i><span class="count"> <?php echo get_post($post->ID)->comment_count; ?></span></a><?php endif; ?>
		<?php if( wpjam_get_setting('wpjam_theme', 'list_like') ) : ?><?php wpjam_theme_postlike2();?><?php endif; ?>
			<a class="share" href="<?php the_permalink(); ?>" data-url="<?php the_permalink(); ?>" data-title="<?php the_title(); ?>" data-thumbnail="<?php echo wpjam_get_post_thumbnail_url($post,array(150,150), $crop=1);?>" data-image="<?php echo wpjam_get_post_thumbnail_url($post,array(1130,848), $crop=1);?>">
			<i class="iconfont icon-icon_share_normal"></i>
			<span>分享</span>
			</a>
	</p>
	<p class="note">
		<?php
			$meta_data = get_post_meta($post->ID, 'post_abstract', true);
			$post_abstract = isset($meta_data) ?$meta_data : '';
			if(!empty($post_abstract)){
				echo mb_strimwidth($post_abstract, 0, 220,"...");
			}else{
				echo mb_strimwidth(strip_tags(apply_filters('the_content', $post->post_content)), 0, 220,"……");
			}
		?>
	</p>
</article>