<?php
function wpjam_xintheme_apis($value){
	$value	= $value?:[];
	
	$value['post.list']	= [
		'json'			=> 'post.list',
		'auth'			=> false,
		'title'			=> '文章列表',
		'modules'		=> wpjam_xintheme_get_post_list_modules()
	];

	$value['post.get']	= [
		'json'			=> 'post.get',
		'auth'			=> false,
		'title'			=> '文章详情',
		'modules'		=> [
			[
				'type'	=> 'post_type',
				'args'	=> '[module action="get" output="post"]'
			]
		]
	];

	$value['post.comment']	= [
		'json'			=> 'post.comment',
		'auth'			=> true,
		'title'			=> '文章评论',
		'modules'		=> [
			[
				'type'	=> 'post_type',
				'args'	=> '[module action="comment"]'
			]
		]
	];

	$value['post.comment.list']	= [
		'json'			=> 'post.comment.list',
		'auth'			=> true,
		'title'			=> '文章评论列表',
		'modules'		=> [
			[
				'type'	=> 'post_type',
				'args'	=> '[module action="comment.list"]'
			]
		]
	];

	$value['post.like']	= [
		'json'			=> 'post.like',
		'auth'			=> true,
		'title'			=> '文章点赞',
		'modules'		=> [
			[
				'type'	=> 'post_type',
				'args'	=> '[module action="like"]'
			]
		]
	];


	$value['post.unlike']	= [
		'json'			=> 'post.unlike',
		'auth'			=> true,
		'title'			=> '文章取消点赞',
		'modules'		=> [
			[
				'type'	=> 'post_type',
				'args'	=> '[module action="unlike"]'
			]
		]
	];

	$value['post.fav']	= [
		'json'			=> 'post.fav',
		'auth'			=> true,
		'title'			=> '文章收藏',
		'modules'		=> [
			[
				'type'	=> 'post_type',
				'args'	=> '[module action="fav"]'
			]
		]
	];

	$value['post.unfav']	= [
		'json'			=> 'post.unfav',
		'auth'			=> true,
		'title'			=> '文章取消收藏',
		'modules'		=> [
			[
				'type'	=> 'post_type',
				'args'	=> '[module action="unfav"]'
			]
		]
	];

	$value['post.fav.list']	= [
		'json'			=> 'post.fav.list',
		'auth'			=> true,
		'title'			=> '文章点赞',
		'modules'		=> [
			[
				'type'	=> 'post_type',
				'args'	=> '[module action="fav.list"]'
			]
		]
	];

	return $value;
}
add_filter('option_wpjam_apis', 'wpjam_xintheme_apis');
add_filter('default_option_wpjam_apis', 'wpjam_xintheme_apis');

function wpjam_xintheme_get_post_list_modules(){
	if(wpjam_get_json() != 'post.list'){
		return [];
	}

	$modules	= [];

	if(empty($_GET['category_id']) && empty($_GET['tag_id']) && empty($_GET['post_author']) && empty($_GET['paged']) && empty($_GET['cursor'])){
		$sticky_posts	= get_option('sticky_posts');
		$modules[]		= [
			'type'	=> 'post_type',
			'args'	=> ['post_type'=>'post', 'action'=>'list', 'posts_per_page'=>10, 'post__in'=>$sticky_posts, 'sticky_posts'=>true, 'output'=>'sticky_posts']
		];
	} 

	$modules[]		= [
		'type'	=> 'post_type',
		'args'	=> ['post_type'=>'post', 'action'=>'list', 'posts_per_page'=>10]
	];

	return $modules;
}


add_filter('wpjam_post_json', function($post_json, $post_id, $args){
	global $post;
	$post	= get_post($post_id);

	$feature_list	= intval(get_post_meta($post_id, 'feature_list', true));
	$post_json['feature_list']	= $feature_list;

	if(!empty($args['sticky_posts'])){
		$size	= [670, 670];
	}else{
		$size	= $feature_list ? [670, 670] : [670,415];
	}
	$post_json['thumbnail']		= wpjam_get_post_thumbnail_url($post,$size);
	
	$post_author	= get_userdata($post->post_author);
	$post_json['author']		= [
		'id'	=> $post_author->ID,
		'user_login'	=> $post_author->user_login,
		'display_name'	=> $post_author->display_name,
		'avatar'		=> get_avatar_url($post_author->user_email, ['size'=>60])
	];

	$post_json['permalink']		= get_permalink($post_id);

	if(is_single()){
		$post_json['content']	= wpjam_content_images($post_json['content'], 1200, false);
		// $post_json['related']	= WPJAM_PostType::get_related($post_id);
	}

	return $post_json;

}, 10, 3);
