<?php
add_filter('wpjam_template', function($wpjam_template, $module){
	if($module == 'user'){
		return get_template_directory().'/user/index.php';
	}
	return $wpjam_template;
}, 10, 2);

add_filter('wpjam_rewrite_rules', function ($rules){
	$rules['user/posts/page/?([0-9]{1,})$']	= 'index.php?module=user&action=posts&paged=$matches[1]';
	$rules['user/([^/]+)$']					= 'index.php?module=user&action=$matches[1]';
	$rules['user/page/?([0-9]{1,})$']		= 'index.php?module=user&action=posts&paged=$matches[1]';
	$rules['user$']							= 'index.php?module=user&action=posts';

	return $rules;
});

if( wpjam_get_setting('wpjam_theme', 'foot_link') ) {
	add_filter('pre_option_link_manager_enabled', '__return_true');	/*激活友情链接后台*/
}

//禁止代码标点转换
remove_filter('the_content', 'wptexturize');
//载入JS\CSS
add_action('wp_enqueue_scripts', function () {
	if (!is_admin()) {
		
		wp_enqueue_style('style', get_stylesheet_directory_uri().'/static/css/style.css');
		wp_enqueue_style('fonts', get_stylesheet_directory_uri().'/static/fonts/iconfont.css');

		wp_deregister_script('jquery');
		wp_enqueue_script('jquery', "https://cdn.staticfile.org/jquery/3.3.1/jquery.min.js", false, null);
		
		wp_deregister_script('jquery-migrate');
		wp_enqueue_script('jquery-migrate', "https://cdn.staticfile.org/jquery-migrate/3.0.1/jquery-migrate.min.js", false, null);

		wp_enqueue_script('autumn',	get_stylesheet_directory_uri() . '/static/js/autumn.min.js', ['jquery'], '', true);
		wp_localize_script('autumn', 'site_url', ['home_url'=>home_url(),'admin_url'=>admin_url('admin-ajax.php')]);

		if (is_singular() && comments_open() && get_option('thread_comments')){
			wp_enqueue_script( 'comment-reply' );
		}

		if(is_single()){
			wp_enqueue_style('fancybox', 'https://cdn.staticfile.org/fancybox/3.4.1/jquery.fancybox.min.css');
			wp_enqueue_script('fancybox3', 'https://cdn.staticfile.org/fancybox/3.4.1/jquery.fancybox.min.js', ['jquery'], '', true);
		}

		global $wp_query; 
			
		wp_enqueue_script( 'xintheme_ajax', get_stylesheet_directory_uri() . '/static/js/ajax.js', ['jquery'], '', true );
		wpjam_localize_script('xintheme_ajax', 'xintheme', [
			'ajaxurl'		=> admin_url('admin-ajax.php'),
			'query'			=> wpjam_json_encode($wp_query->query),
			'current_page'	=> get_query_var('paged') ?: 1,
			'max_page'		=> $wp_query->max_num_pages ?? 0,
			'paging_type'	=> wpjam_theme_get_setting('paging_xintheme')
		]);
	}	
});

// //删除菜单多余css class
// function wpjam_css_attributes_filter($classes) {
// 	return is_array($classes) ? array_intersect($classes, array('current-menu-item','current-post-ancestor','current-menu-ancestor','current-menu-parent','menu-item-has-children','menu-item')) : '';
// }
// add_filter('nav_menu_css_class',	'wpjam_css_attributes_filter', 100, 1);
// add_filter('nav_menu_item_id',		'wpjam_css_attributes_filter', 100, 1);
// add_filter('page_css_class', 		'wpjam_css_attributes_filter', 100, 1);

add_filter('body_class',function ($classes) {
	//固定导航
	if ( wpjam_get_setting('wpjam_theme', 'navbar_sticky') ){
		$classes[]	= 'navbar-sticky';
	}

	//暗黑风格
	if(wpjam_get_setting('wpjam_theme', 'dark_mode')){
		$classes[]	= 'dark-mode';
	}

	return $classes;
});

//删除wordpress默认相册样式
add_filter( 'use_default_gallery_style', '__return_false' );

add_filter('wpjam_post_thumbnail_url', function($post_thumbnail_uri, $post){
	if(get_post_meta($post->ID, 'header_img', true)){
		return get_post_meta($post->ID, 'header_img', true);
	}elseif($post_thumbnail_uri){
		return $post_thumbnail_uri;
	}else{
		return wpjam_get_post_first_image($post->post_content);
	}
},10,2);

add_filter('wpjam_default_thumbnail_uri',function ($default_thumbnail){
	$default_thumbnails	= wpjam_get_setting('wpjam_theme', 'thumbnails');

	if($default_thumbnails){
		shuffle($default_thumbnails);
		return $default_thumbnails[0];
	}else{
		return $default_thumbnail;
	}
},99);

/* 评论作者链接新窗口打开 */
add_filter('get_comment_author_link', function () {
	$url	= get_comment_author_url();
	$author = get_comment_author();
	if ( empty( $url ) || 'http://' == $url ){
		return $author;
	}else{
		return "<a target='_blank' href='$url' rel='external nofollow' class='url'>$author</a>";
	}
});

//文章自动nofollow
add_filter( 'the_content', function ( $content ) {
	//fancybox3图片添加 data-fancybox
	global $post;
	$pattern = "/<a(.*?)href=('|\")([^>]*).(bmp|gif|jpeg|jpg|png|swf)('|\")(.*?)>(.*?)<\/a>/i";
	$replacement = '<a$1href=$2$3.$4$5 data-fancybox="images" $6>$7</a>';
	$content = preg_replace($pattern, $replacement, $content);
	$content = str_replace(']]>', ']]>', $content);
	return $content;
});

add_filter('the_excerpt',function($post_excerpt){
	global $post;
	if($post_abstract = get_post_meta($post->ID, 'post_abstract', true)){
		return mb_strimwidth($post_abstract, 0, 115, '...');
	}else{
		return get_post_excerpt($post, 115);
	}
});

//修复 WordPress 找回密码提示“抱歉，该key似乎无效”
add_filter('retrieve_password_message', function ( $message, $key ) {
	if ( strpos($_POST['user_login'], '@') ) {
		$user_data = get_user_by('email', trim($_POST['user_login']));
	} else {
		$login = trim($_POST['user_login']);
		$user_data = get_user_by('login', $login);
	}
	
	$user_login = $user_data->user_login;
	$msg	= __('有人要求重设如下帐号的密码：'). "\r\n\r\n";
	$msg	.= network_site_url() . "\r\n\r\n";
	$msg	.= sprintf(__('用户名：%s'), $user_login) . "\r\n\r\n";
	$msg	.= __('若这不是您本人要求的，请忽略本邮件，一切如常。') . "\r\n\r\n";
	$msg	.= __('要重置您的密码，请打开下面的链接：'). "\r\n\r\n";
	$msg	.= network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') ;

	return $msg;
}, null, 2);


//禁止FEED
if( wpjam_get_setting('wpjam_theme', 'xintheme_feed') ) {
	function wpjam_disable_feed() {
		wp_die(__('<h1>Feed已经关闭, 请访问网站<a href="'.get_bloginfo('url').'">首页</a>!</h1>'));
	}

	add_action('do_feed',		'wpjam_disable_feed', 1);
	add_action('do_feed_rdf',	'wpjam_disable_feed', 1);
	add_action('do_feed_rss',	'wpjam_disable_feed', 1);
	add_action('do_feed_rss2',	'wpjam_disable_feed', 1);
	add_action('do_feed_atom',	'wpjam_disable_feed', 1);
}

// 使用自定义头像
add_filter('pre_get_avatar_data', function($args, $id_or_email){
	if(is_numeric($id_or_email)){
		if($photo	= get_user_meta($id_or_email, 'photo', true)){
			$args['url'] = wpjam_get_thumbnail($photo, [$args['width'], $args['height']]);
		}
	}

	return $args;
},10,2);

//使用v2ex镜像avatar头像
if( wpjam_get_setting('wpjam_theme', 'xintheme_v2ex') ) {
	add_filter('get_avatar', function ($avatar, $id_or_email, $size){
		return str_replace(['cn.gravatar.com/avatar', 'secure.gravatar.com/avatar', '0.gravatar.com/avatar', '1.gravatar.com/avatar', '2.gravatar.com/avatar'], 'cdn.v2ex.com/gravatar', $avatar);
	}, 10, 3 );
}

add_action('wp_head', function (){
	if(is_singular()) { 
		global $post;
		wpjam_update_post_views($post->ID);
	}
}); 

function wpjam_login_default_settings($value){
	if(!isset($value['login_head'])){
		$value['login_head']	= '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('template_directory') . '/static/css/login.css" />'."\n";
		$value['login_head'] .= '<script type="text/javascript" src="//dn-staticfile.qbox.me/jquery/2.1.4/jquery.min.js"></script>'."\n";
	}

	if(!isset($value['login_footer'])){
		$value['login_footer'] = '
		<div class="footer">
		<p>Copyright &copy; '.date('Y').' All Rights | Powered by <a href="http://www.xintheme.com" target="_blank">XinTheme</a>&nbsp;+&nbsp;<a href="https://blog.wpjam.com/" target="_blank">WordPress 果酱</a></p>
		</div>
		<script type="text/javascript" src="'.get_bloginfo('template_directory').'/static/js/resizeBg.js"></script>
		<script type="text/javascript">
		jQuery("body").prepend(\'<div class="loading"><img src="'.get_bloginfo('template_directory').'/static/images/login_loading.gif" width="58" height="10"></div><div id="bg"><img /></div>\')
		jQuery(\'#bg\').children(\'img\').attr(\'src\', \''.get_bloginfo('template_directory').'/static/images/login_bg.jpg\').load(function(){
			resizeImage(\'bg\');
		jQuery(window).bind("resize", function() { resizeImage(\'bg\'); });
		jQuery(\'.loading\').fadeOut();
		});
		</script>
		';
	}

	if( wpjam_get_setting('wpjam_theme', 'xintheme_article') ) {
		$value['login_redirect']	= home_url("/wp-admin/edit.php");
	}

	return $value;
}
add_filter('option_wpjam-basic', 'wpjam_login_default_settings');
add_filter('default_option_wpjam-basic', 'wpjam_login_default_settings');

//在[媒体库]只显示用户上传的文件
add_filter('parse_query',	function( $wp_query ) {
	if ( strpos( $_SERVER[ 'REQUEST_URI' ], '/wp-admin/upload.php' ) !== false ) {
		if ( !current_user_can( 'manage_options' ) && !current_user_can( 'manage_media_library' ) ) {
			global $current_user;
			$wp_query->set( 'author', $current_user->id );
		}
	}
});

add_action('pre_get_posts',	function($wp_query) {
	global $current_user, $pagenow;

	if($wp_query->is_main_query()){
		if(is_module('user', 'posts')){
			$wp_query->set('ignore_sticky_posts', true);
			$wp_query->set('post_type', 'post');
			$wp_query->set('post_status', 'any');
			$wp_query->set('author',	$current_user->ID);
		}elseif(is_home()){
			$wp_query->set('ignore_sticky_posts',true);
		}elseif(is_search()){
			$wp_query->set('post_type', 'post');	//搜索结果排除所有页面
		}
	}elseif($pagenow == 'admin-ajax.php' && $_REQUEST['action'] == 'query-attachments'){	//在文章编辑页面的[添加媒体]只显示用户自己上传的文件
		if(current_user_can('manage_options') && current_user_can('manage_media_library')){
			$wp_query->set('author', $current_user->ID );
		}
	}

	return $wp_query;
});

/* 搜索关键词为空 */
add_filter( 'request', function ( $query_variables ) {
	if (isset($_GET['s']) && !is_admin()) {
		if (empty($_GET['s']) || ctype_space($_GET['s'])) {
			wp_redirect( home_url() );
			exit;
		}
	}
	return $query_variables;
});